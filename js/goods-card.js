$(document).ready(function () {
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    // centerMode: true,
    focusOnSelect: true,
    prevArrow: '<div class="slick__prev-arrow"><img src="icons/top-sleder__left-arrow.png" alt="button-left"></div>',
    nextArrow: '<div class="slick__next-arrow"><img src="icons/top-sleder__right-arrow.png" alt="button-right"></div>',
    centerPadding: '40px',
    responsive: [{
      breakpoint: 549,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }]

  });


  var complectsCardFirst = new Swiper('.complects__card--tab-1 .swiper-container', {
    slidesPerView: 1,
    loop: true,
    navigation: {
      prevEl: '.complects__card-item-swiper-left',
      nextEl: '.complects__card-item-swiper-right',
    },
  });

  var complectsCardSecond = new Swiper('.complects__card--tab-2 .swiper-container', {
    slidesPerView: 1,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.complects__card-item-swiper-left--tab-2',
      nextEl: '.complects__card-item-swiper-right--tab-2',
    },
  });

  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-hit-left',
      nextEl: '.product-tabs-hit-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });
  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit2', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-hit2-left',
      nextEl: '.product-tabs-hit2-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });
  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit3', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-hit3-left',
      nextEl: '.product-tabs-hit3-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });
  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit4', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-hit4-left',
      nextEl: '.product-tabs-hit4-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });
  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit5', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-hit5-left',
      nextEl: '.product-tabs-hit5-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });

  var swiperNews = new Swiper('.carousel-news .swiper-container', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    navigation: {
      prevEl: '.product-new-left',
      nextEl: '.product-new-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });

  // var complectsCardFirst = new Swiper('.complects__card--tab-1 .swiper-container', {
  //   slidesPerView: 1,
  //   loop: true,
  //   navigation: {
  //     prevEl: '.complects__card-item-swiper-left',
  //     nextEl: '.complects__card-item-swiper-right',
  //   },
  //   // spaceBetween: 50,
  // });
  // var complectsCardSecond = new Swiper('.complects__card--tab-2 .swiper-container', {
  //   slidesPerView: 1,
  //   loop: true,
  //   navigation: {
  //     prevEl: '.complects__card-item-swiper-left--tab-2',
  //     nextEl: '.complects__card-item-swiper-right--tab-2',
  //   },
  //   // spaceBetween: 50,
  // });


  loadpage();

  document.onkeydown = function (evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
      isEscape = (evt.key == "Escape" || evt.key == "Esc");
    } else {
      isEscape = (evt.keyCode == 27);
    }
    if (isEscape) {
      document.getElementsByTagName('HTML')[0].classList.toggle('debug');
    }
  };

  var swiperPartner = new Swiper('.partner-carousel .swiper-container', {});

  /*Tabs */
  function tabWidth() {
    var tabWidth = $('.tabs__header--title.active').width();
    $('.js-tabs-underline').css('width', tabWidth + 'px');
  };

  $('.js-tabs-title').on('click', function () {
    console.log('приветт');
    var openTab = $(this).data('tab'),
      linePosition = $(this).position().left;

    $('.js-tabs-underline').css('transform', 'translateX(' + linePosition + 'px)');
    $('.js-tabs-title').removeClass('active');
    $(this).addClass('active');
    $('.js-tabs-content').removeClass('active');
    $(openTab).addClass('active');
    tabWidth();
  });

  /*Tabs 2*/
  function tabWidth2() {
    var tabWidth2 = $('.js-tabs-title2.active').width();
    $('.js-tabs-underline2').css('width', tabWidth2 + 'px');
  };

  $('.js-tabs-title2').on('click', function () {
    var openTab = $(this).data('tab'),
      linePosition2 = $(this).position().left;

    $('.js-tabs-underline2').css('transform', 'translateX(' + linePosition2 + 'px)');
    $('.js-tabs-title2').removeClass('active');
    $(this).addClass('active');
    $('.js-tabs-content2').removeClass('active');
    $(openTab).addClass('active');
    tabWidth2();
  });

  /*Tabs 3*/
  function tabWidth3() {
    var tabWidth3 = $('.js-tabs-title3.active').width();
    $('.js-tabs-underline3').css('width', tabWidth3 + 'px');
  };

  $('.js-tabs-title3').on('click', function () {
    var openTab = $(this).data('tab'),
      linePosition3 = $(this).position().left;

    $('.js-tabs-underline3').css('transform', 'translateX(' + linePosition3 + 'px)');
    $('.js-tabs-title3').removeClass('active');
    $(this).addClass('active');
    $('.js-tabs-content3').removeClass('active');
    $(openTab).addClass('active');
    tabWidth3();
  });




  $('#goods__tabs-button-1').on('click', function () {
    $('#tabs__header-1').toggleClass('tabs__header--show');
    $(this).toggleClass('goods__tabs-button--close');
  });
  $('#goods__tabs-button-2').on('click', function () {
    $('#tabs__header-2').toggleClass('tabs__header--show');
    $(this).toggleClass('goods__tabs-button--close');
  });

  $('.description__link-1').on('click', function () {
    var el = $(this);
    var dest = el.attr('href'); // получаем направление
    if (dest !== undefined && dest !== '') { // проверяем существование
      $('html').animate({
          scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
        }, 1500 // скорость прокрутки
      );
    }
    return false;
  });

});