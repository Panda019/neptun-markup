var scrollTops = [
  {
    top : $('.main-popular-category').offset().top - ($(window).height() / 2),
    func : function() {
      TweenMax.staggerFromTo('.main-popular-category .main-popular-left', 2,
                       {ease : Power4.easeOut, opacity : 0, x: -300},
                       {ease : Power4.easeOut, opacity : 1, x: 0}, 0.3);
      TweenMax.staggerFromTo('.main-popular-category .main-popular-right', 2,
                       {ease : Power4.easeOut, opacity : 0, x: 300},
                       {ease : Power4.easeOut, opacity : 1, x: 0}, 0.3);
    }
  },
  {
    top : $('.main-videos').offset().top - ($(window).height() / 2),
    func : function() {
      TweenMax.staggerFromTo('.main-videos .video', 2,
                             {ease : Power4.easeOut, opacity : 0},
                             {ease : Power4.easeOut, opacity : 1}, 0.3);
    }
  },
  {
    top : $('.main-partners').offset().top - ($(window).height() / 2),
    func : function() {
      TweenMax.staggerFromTo('.animate-partner', 1,
                             {ease : Power4.easeOut, opacity : 0},
                             {ease : Power4.easeOut, opacity : 1, delay: 1}, 0.1);
      TweenMax.staggerFromTo('.animate-partner-left', 1,
                             {ease : Power4.easeOut, x: -100, opacity : 0},
                             {ease : Power4.easeOut, x: 0, opacity : 1}, 0.3);
    }
  }
];

function loadpage() {
  TweenMax.fromTo('#top-header', 2, {ease : Power4.easeOut, opacity : 0, y : -100},
                  {ease : Power4.easeOut, opacity : 1, y : 0, delay: 1});
  // TweenMax.fromTo('#bottom-header', 4, {ease : Power4.easeOut, opacity : 0},
  //                 {ease : Power4.easeOut, opacity : 1, delay: 1});
  // TweenMax.staggerFromTo('.animate-header', 3,
  //                            {ease : Power4.easeOut, opacity : 0},
  //                            {ease : Power4.easeOut, opacity : 1}, 0.3);
  // TweenMax.fromTo('.promo', 2, {ease : Power4.easeOut, opacity : 0},
  //                 {ease : Power4.easeOut, opacity : 1, delay: 2});
  // TweenMax.staggerFromTo('.benefits .column', 3,
  //                            {ease : Power4.easeOut, opacity : 0, y: 100},
  //                            {ease : Power4.easeOut, opacity : 1, y: 0, delay: 2}, 0.3);

}

$(window).scroll(function() {
  var scroll = $(this).scrollTop();
  $(scrollTops).each(function(i, item) {
    if (scroll > item.top && !item.runed) {
      item.func();
      item.runed = true;
    }
  });
});