$(document).ready(function () {

  var globalWidth = window.screen.width;

  $('#popup-main-menu-start-mobile').on('click', function () {
    TweenMax.fromTo('#popup-main-main-menu', 1, {
      ease: Power4.easeOut,
      left: -globalWidth
    }, {
      ease: Power4.easeOut,
      left: 0
    });
  });

  $('#popup-main-menu-mobile-back').on('click', function () {
    TweenMax.fromTo('#popup-main-main-menu', 1, {
      ease: Power4.easeOut,
      left: 0
    }, {
      ease: Power4.easeOut,
      left: -globalWidth
    });
  });

  $('#popup-catalog-menu-start-mobile-button').on('click', function () {
    TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
      ease: Power4.easeOut,
      left: -globalWidth
    }, {
      ease: Power4.easeOut,
      left: 0
    });
  });

  $('#popup-catalog-menu-start-mobile-button--bottom-header').on('click', function () {
    TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
      ease: Power4.easeOut,
      left: -globalWidth
    }, {
      ease: Power4.easeOut,
      left: 0
    });
  });

  $('#popup-catalog-menu-mobile-close').on('click', function () {
    TweenMax.fromTo('#popup-catalog-menu-mobile', 1, {
      ease: Power4.easeOut,
      left: 0
    }, {
      ease: Power4.easeOut,
      left: -globalWidth
    });
  });

  $('#popup-catalog-menu-start-mobile-close').on('click', function () {
    TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
      ease: Power4.easeOut,
      left: 0
    }, {
      ease: Power4.easeOut,
      left: -globalWidth
    });
  });

  $('#popup-catalog-menu-mobile-close').on('click', function () {
    TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
      ease: Power4.easeOut,
      left: 0
    }, {
      ease: Power4.easeOut,
      left: -globalWidth
    });
  });

  $('#popup-catalog-menu-start-mobile').on('click', '.section', function () {
    TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
      ease: Power4.easeOut,
      left: 0
    }, {
      ease: Power4.easeOut,
      left: -globalWidth
    });
    TweenMax.fromTo('#popup-catalog-menu-mobile', 1, {
      ease: Power4.easeOut,
      left: globalWidth
    }, {
      ease: Power4.easeOut,
      left: 0
    });
  });

  $('#popup-catalog-menu-mobile-back').on('click', function () {
    TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
      ease: Power4.easeOut,
      left: -globalWidth
    }, {
      ease: Power4.easeOut,
      left: 0
    });
    TweenMax.fromTo('#popup-catalog-menu-mobile', 1, {
      ease: Power4.easeOut,
      left: 0
    }, {
      ease: Power4.easeOut,
      left: globalWidth
    });
  });


  $('.button__open-menu').click(function () {
    if ($(window).width() < 551) {
      TweenMax.fromTo('#popup-catalog-menu-start-mobile', 1, {
        ease: Power4.easeOut,
        left: -globalWidth
      }, {
        ease: Power4.easeOut,
        left: 0
      });
    } else {
      $('.button__open-menu').toggleClass('button__open-menu--white');
      $('.icon-burger').toggleClass('icon-burger--cross');
      $('.catalog-menu-popup').toggleClass('catalog-menu-popup--show');
    }
  });
  $(document).mouseup(function (e) {
    var container = $(".catalog-menu-popup"),
      burger = $(".button__open-menu");

    console.log(burger[0].contains(e.target))
    console.log(burger.has(e.target).length) // <----- Firefox === 0, Chrome === 1. WTF?

    if (!burger[0].contains(e.target) && !container[0].contains(e.target))  { // <----- THIS
      if (container.hasClass('catalog-menu-popup--show')) {
        container.removeClass("catalog-menu-popup--show");
        $('.button__open-menu').removeClass('button__open-menu--white');
        $('.icon-burger').removeClass('icon-burger--cross');
      }
    }
  });


  $('.navbar-link').on('click', function () {
    $('.all-sales-popup').toggleClass('all-sales-popup--show');
  });
  $(document).mouseup(function (e) {
    var containerMenu = $(".all-sales-popup"),
      buttonPink = $(".navbar-link");
    if (containerMenu.has(e.target).length === 0 && buttonPink.has(e.target).length === 0) {
      containerMenu.removeClass("all-sales-popup--show");
    }
  });


  $('.categories__list-item').on('click', function () {
    var openTab2 = $(this).data('cat');
    $('.categories__list-item').removeClass('categories__list-item--active');
    $(this).addClass('categories__list-item--active');
    $('.categories__content').removeClass('categories__content--active');
    $(openTab2).addClass('categories__content--active');
  });

  $('#current-region__open-change-region').on('click', function () {
    $('#current-region__popup').css('display', 'block');
  });


  $('.breadcrumbs__item-dropdown').on('click', function () {
    $('.breadcrumbs__item-dropdown-wrap').not($(this).children()).addClass('breadcrumbs__item-dropdown-wrap--close');
    $(this).children('.breadcrumbs__item-dropdown-wrap').toggleClass('breadcrumbs__item-dropdown-wrap--close');
  });
  $(document).mouseup(function (e) {
    var containerMenu = $(".breadcrumbs__item-dropdown-wrap"),
      buttonPink = $(".breadcrumbs__item-dropdown");
    if (containerMenu.has(e.target).length === 0 && buttonPink.has(e.target).length === 0) {
      containerMenu.addClass("breadcrumbs__item-dropdown-wrap--close");
    }
  });




  $('.slider-toolbar').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<div class="toolbar-bottom__slider-prev-arrow"></div>',
    nextArrow: '<div class="toolbar-bottom__slider-next-arrow"></div>',
  });

  $('.toolbar-bottom__slider-slide').hover(function () {
    var openSliderInfo = $(this).data('slick-index');
    var slidCard = $('.toolbar-bottom__slider-info').each((index, item) => index === openSliderInfo ? item.style.opacity = 1 : item.style.opacity = 0);
    slidCard.find($('.slider-info__counter-counter')).html(counter);
  });

  $(document).mouseup(function (e) {
    var containerMenu = $(".toolbar-bottom__slider-info");
    if (containerMenu.has(e.target).length === 0) {
      containerMenu.css('opacity', '0');
    }
  });


  var counter = 0
  console.log(counter);
  // $('.slider-info__counter-counter').html(counter);
  $('.slider-info__counter-plus').on('click', function () {
    counter += 1;
    $('.slider-info__counter-counter').html(counter);
  });
  $('.slider-info__counter-minus').on('click', function () {
    counter -= 1;
    $('.slider-info__counter-counter').html(counter);
  });

  $('.toolbar-bottom__callback-toggle').on('click', function () {
    $('.toolbar-bottom__callback-dropdown').toggleClass('toolbar-bottom__callback-dropdown--close');
  });
  $(document).mouseup(function (e) {
    var containerMenu = $(".toolbar-bottom__callback-dropdown"),
      buttonPink = $(".toolbar-bottom__callback-toggle");
    if (containerMenu.has(e.target).length === 0 && buttonPink.has(e.target).length === 0) {
      containerMenu.addClass("toolbar-bottom__callback-dropdown--close");
    }
  });








  // var complectsCardFirst = new Swiper('.complects__card--tab-1 .swiper-container', {
  //   slidesPerView: 1,
  //   loop: true,
  //   navigation: {
  //     prevEl: '.complects__card-item-swiper-left',
  //     nextEl: '.complects__card-item-swiper-right',
  //   },
  // });

  // var complectsCardSecond = new Swiper('.complects__card--tab-2 .swiper-container', {
  //   slidesPerView: 1,
  //   loop: true,
  //   observer: true,
  //   observeParents: true,
  //   navigation: {
  //     prevEl: '.complects__card-item-swiper-left--tab-2',
  //     nextEl: '.complects__card-item-swiper-right--tab-2',
  //   },
  // });

  // var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit', {
  //   slidesPerView: 4,
  //   spaceBetween: 30,
  //   loop: true,
  //   observer: true,
  //   observeParents: true,
  //   navigation: {
  //     prevEl: '.product-tabs-hit-left',
  //     nextEl: '.product-tabs-hit-right',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });
  // var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit2', {
  //   slidesPerView: 4,
  //   spaceBetween: 30,
  //   loop: true,
  //   observer: true,
  //   observeParents: true,
  //   navigation: {
  //     prevEl: '.product-tabs-hit2-left',
  //     nextEl: '.product-tabs-hit2-right',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });
  // var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit3', {
  //   slidesPerView: 4,
  //   spaceBetween: 30,
  //   loop: true,
  //   observer: true,
  //   observeParents: true,
  //   navigation: {
  //     prevEl: '.product-tabs-hit3-left',
  //     nextEl: '.product-tabs-hit3-right',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });
  // var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit4', {
  //   slidesPerView: 4,
  //   spaceBetween: 30,
  //   loop: true,
  //   observer: true,
  //   observeParents: true,
  //   navigation: {
  //     prevEl: '.product-tabs-hit4-left',
  //     nextEl: '.product-tabs-hit4-right',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });
  // var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit5', {
  //   slidesPerView: 4,
  //   spaceBetween: 30,
  //   loop: true,
  //   observer: true,
  //   observeParents: true,
  //   navigation: {
  //     prevEl: '.product-tabs-hit5-left',
  //     nextEl: '.product-tabs-hit5-right',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });

  //   var swiperNews = new Swiper('.carousel-news .swiper-container', {
  //       slidesPerView: 4,
  //       spaceBetween: 30,
  //       loop: true,
  //       navigation: {
  //         prevEl: '.product-new-left',
  //         nextEl: '.product-new-right',
  //       },
  //       breakpoints: {
  //         1200: {
  //           slidesPerView: 3
  //         },
  //         768: {
  //           spaceBetween: 0,
  //           slidesPerView: 3
  //         },
  //         550: {
  //           spaceBetween: 0,
  //           slidesPerView: 2
  //         }
  //       }
  //     });

  //     // var complectsCardFirst = new Swiper('.complects__card--tab-1 .swiper-container', {
  //     //   slidesPerView: 1,
  //     //   loop: true,
  //     //   navigation: {
  //     //     prevEl: '.complects__card-item-swiper-left',
  //     //     nextEl: '.complects__card-item-swiper-right',
  //     //   },
  //     //   // spaceBetween: 50,
  //     // });
  //     // var complectsCardSecond = new Swiper('.complects__card--tab-2 .swiper-container', {
  //     //   slidesPerView: 1,
  //     //   loop: true,
  //     //   navigation: {
  //     //     prevEl: '.complects__card-item-swiper-left--tab-2',
  //     //     nextEl: '.complects__card-item-swiper-right--tab-2',
  //     //   },
  //     //   // spaceBetween: 50,
  //     // });


  //     loadpage();

  //     document.onkeydown = function(evt) {
  //       evt = evt || window.event;
  //       var isEscape = false;
  //       if ("key" in evt) {
  //           isEscape = (evt.key == "Escape" || evt.key == "Esc");
  //       } else {
  //           isEscape = (evt.keyCode == 27);
  //       }
  //       if (isEscape) {
  //         document.getElementsByTagName('HTML')[0].classList.toggle('debug');
  //       }
  //   };

  //     var swiperPartner = new Swiper('.partner-carousel .swiper-container', {});

  //   /*Tabs */
  //   function tabWidth() {
  //     var tabWidth = $('.tabs__header--title.active').width();
  //     $('.js-tabs-underline').css('width', tabWidth + 'px');
  //   };

  //   $('.js-tabs-title').on('click', function() {
  //     console.log('приветт');
  //     var openTab = $(this).data('tab'), linePosition = $(this).position().left; 

  //     $('.js-tabs-underline').css('transform', 'translateX(' + linePosition + 'px)');
  //     $('.js-tabs-title').removeClass('active');
  //     $(this).addClass('active');
  //     $('.js-tabs-content').removeClass('active');
  //     $(openTab).addClass('active');
  //     tabWidth();
  //   });

  //   /*Tabs 2*/
  //   function tabWidth2() {
  //     var tabWidth2 = $('.js-tabs-title2.active').width();
  //     $('.js-tabs-underline2').css('width', tabWidth2 + 'px');
  //   };

  //   $('.js-tabs-title2').on('click', function() {
  //     var openTab = $(this).data('tab'), linePosition2 = $(this).position().left; 

  //     $('.js-tabs-underline2').css('transform', 'translateX(' + linePosition2 + 'px)');
  //     $('.js-tabs-title2').removeClass('active');
  //     $(this).addClass('active');
  //     $('.js-tabs-content2').removeClass('active');
  //     $(openTab).addClass('active');
  //     tabWidth2();
  //   });

  //    /*Tabs 3*/
  //    function tabWidth3() {
  //     var tabWidth3 = $('.js-tabs-title3.active').width();
  //     $('.js-tabs-underline3').css('width', tabWidth3 + 'px');
  //   };

  //   $('.js-tabs-title3').on('click', function() {
  //     var openTab = $(this).data('tab'), linePosition3 = $(this).position().left; 

  //     $('.js-tabs-underline3').css('transform', 'translateX(' + linePosition3 + 'px)');
  //     $('.js-tabs-title3').removeClass('active');
  //     $(this).addClass('active');
  //     $('.js-tabs-content3').removeClass('active');
  //     $(openTab).addClass('active');
  //     tabWidth3();
  //   });




  //   $('#goods__tabs-button-1').on('click',function(){
  //     $('#tabs__header-1').toggleClass('tabs__header--show');
  //     $(this).toggleClass('goods__tabs-button--close');
  //   });
  //   $('#goods__tabs-button-2').on('click',function(){
  //     $('#tabs__header-2').toggleClass('tabs__header--show');
  //     $(this).toggleClass('goods__tabs-button--close');
  //   });

  // $('.description__link-1').on( 'click', function(){ 
  //     var el = $(this);
  //     var dest = el.attr('href'); // получаем направление
  //     if(dest !== undefined && dest !== '') { // проверяем существование
  //         $('html').animate({ 
  //             scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
  //         }, 1500 // скорость прокрутки
  //         );
  //     }
  //     return false;
  // });
});