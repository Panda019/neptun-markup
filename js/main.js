$(document).ready(function () {
  var swiper = new Swiper('.promo .swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      navigation: {
        nextEl: '.promo-right',
        prevEl: '.promo-left'
      },
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + '</span>';
        // return '<span class="' + className + '">' + (index + 1) + '</span>';
      },
    }
  });

  var swiperNews = new Swiper('.carousel-news .swiper-container', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    navigation: {
      prevEl: '.product-new-left',
      nextEl: '.product-new-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });

  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-hit', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-hit-left',
      nextEl: '.product-tabs-hit-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });

  var swiperTabs = new Swiper('.carousel-tabs .swiper-container-sell', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    observer: true,
    observeParents: true,
    navigation: {
      prevEl: '.product-tabs-sell-left',
      nextEl: '.product-tabs-sell-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });

  var swiperTabs = new Swiper('.carousel-showroom .swiper-container', {
    slidesPerView: 4,
    spaceBetween: 30,
    loop: true,
    // observer: true,
    // observeParents: true,
    navigation: {
      prevEl: '.product-showroom-left',
      nextEl: '.product-showroom-right',
    },
    breakpoints: {
      1200: {
        slidesPerView: 3
      },
      768: {
        spaceBetween: 0,
        slidesPerView: 3
      },
      550: {
        spaceBetween: 0,
        slidesPerView: 2
      }
    }
  });

  // var swiperTabs = new Swiper('.carousel-news--orange .swiper-container', {
  //   slidesPerView: 3,
  //   spaceBetween: 30,
  //   loop: true,
  //   // observer: true,
  //   // observeParents: true,
  //   navigation: {
  //     prevEl: '.arrow-left--orange',
  //     nextEl: '.arrow-right--orange',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     1199: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });

  // var swiperTabs = new Swiper('.carousel-news--green .swiper-container', {
  //   slidesPerView: 3,
  //   spaceBetween: 30,
  //   loop: true,
  //   // observer: true,
  //   // observeParents: true,
  //   navigation: {
  //     prevEl: '.arrow-left--green',
  //     nextEl: '.arrow-right--green',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     1199: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });

  // var swiperTabs = new Swiper('.carousel-news--pink .swiper-container--pink', {
  //   slidesPerView: 3,
  //   spaceBetween: 30,
  //   loop: true,
  //   // observer: true,
  //   // observeParents: true,
  //   navigation: {
  //     prevEl: '.arrow-left--pink',
  //     nextEl: '.arrow-right--pink',
  //   },
  //   breakpoints: {
  //     1200: {
  //       slidesPerView: 3
  //     },
  //     1199: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     768: {
  //       spaceBetween: 0,
  //       slidesPerView: 3
  //     },
  //     550: {
  //       spaceBetween: 0,
  //       slidesPerView: 2
  //     }
  //   }
  // });

  var swiperVideos = new Swiper('.main-videos .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 50,
    loop: true,
    navigation: {
      prevEl: '.main-videos-left',
      nextEl: '.main-videos-right',
    },
    breakpoints: {
      768: {
        slidesPerView: 1
      }
    }
  });

  // var swiperVideos = new Swiper('.main-videos .swiper-container', {
  //   slidesPerView: 2,
  //   spaceBetween: 50,
  //   breakpoints: {
  //     768: {
  //       slidesPerView: 1
  //     }
  //   }
  // });

  var swiperVideos = new Swiper('.toolbar-bottom__slider .swiper-container', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
      prevEl: '.main-videos-left',
      nextEl: '.main-videos-right',
    },
    breakpoints: {
      768: {
        slidesPerView: 1
      }
    }
  });

  var swiperReviews = new Swiper('.goods__review-swiper', {
    slidesPerView: 1,
    loop: true,
    navigation: {
      prevEl: '.product-new-filter-left',
      nextEl: '.product-new-filter-right',
    },
    // spaceBetween: 50,
  });

  loadpage();

  document.onkeydown = function (evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
      isEscape = (evt.key == "Escape" || evt.key == "Esc");
    } else {
      isEscape = (evt.keyCode == 27);
    }
    if (isEscape) {
      document.getElementsByTagName('HTML')[0].classList.toggle('debug');
    }
  };

  var swiperPartner = new Swiper('.partner-carousel .lorem', {});








  function addProgressBar(page) {
    page = page === -1 ? 0 : page
    var elems = $(".owl-dot")

    if (elems.length === 0) return

    var target = elems[page]

    target.innerHTML = '<span><div class="skill12"></div></span>'

    var bar = new ProgressBar.Circle($(target).find('.skill12')[0], {
      opacity: 0.5,
      color: "rgb(255,255,255)",
      trailColor: 'rgba(255,255,255,0.5)',
      from: {
        color: 'rgba(255,255,255,0.5)',
        width: 10
      },
      to: {
        color: 'rgba(255,255,255,0.5)',
        width: 10
      },
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 10,
      rtl: false,
      trailWidth: 10,
      easing: 'easeInOut',
      duration: 5000,
      text: {
        autoStyleContainer: false
      },
      from: {
        color: '#fff ',
        width: 10
      },
      to: {
        color: '#fff',
        width: 10
      },
      // Set default step function for all animate calls
      step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }

      }
    });
    bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
    bar.text.style.fontSize = '0';

    bar.animate(1.0) // Number from 0.0 to 1.0
    // console.log(elem);
  }


  var owl = $('.owl-carousel__progressbar').owlCarousel({
    loop: true,
    smartSpeed: 700,
    nav: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    },
    onInitialized: function (event) {
      console.log(event)
      addProgressBar(event.page.index)
    },
    onChanged: function (event) {
      console.log(event)
      addProgressBar(event.page.index)
    }
  });

  $('.play').on('click', function () {
    owl.trigger('play.owl.autoplay', [1000])
  });
  $('.stop').on('click', function () {
    owl.trigger('stop.owl.autoplay')
  });




  function tabWidth() {
    var tabWidth = $('.tabs__header--title.active').width();
    $('.js-tabs-underline').css('width', tabWidth + 'px');
  };

  $('.js-tabs-title').on('click', function () {
    var openTab = $(this).data('tab'),
      linePosition = $(this).position().left;
    $('.js-tabs-underline').css('transform', 'translateX(' + linePosition + 'px)');
    $('.js-tabs-title').removeClass('active');
    $(this).addClass('active');
    $('.js-tabs-content').removeClass('active');
    $(openTab).addClass('active');
    tabWidth();
  });

  // $('.slider-toolbar').slick({
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   prevArrow: '<div class="toolbar-bottom__slider-prev-arrow"></div>',
  //   nextArrow: '<div class="toolbar-bottom__slider-next-arrow"></div>',  
  // });

  // $('.toolbar-bottom__slider-slide').hover(function() {
  //   var openSliderInfo = $(this).data('slick-index');
  //   var slidCard = $('.toolbar-bottom__slider-info').each((index, item) => index === openSliderInfo ? item.style.opacity= 1 : item.style.opacity= 0 );
  //   slidCard.find($('.slider-info__counter-counter')).html(counter);
  // });

  //   $(document).mouseup(function (e) {
  //     var containerMenu = $(".toolbar-bottom__slider-info");
  //       if (containerMenu.has(e.target).length === 0) {
  //         containerMenu.css('opacity', '0');
  //         }     
  //       });


  // var counter = 0
  // console.log(counter);
  // // $('.slider-info__counter-counter').html(counter);
  // $('.slider-info__counter-plus').on('click', function() {
  //   counter += 1;
  //   $('.slider-info__counter-counter').html(counter);
  // });
  // $('.slider-info__counter-minus').on('click', function() {
  //   counter -= 1;
  //   $('.slider-info__counter-counter').html(counter);
  // });

  // $('.toolbar-bottom__callback-toggle').on('click',function(){
  //   $('.toolbar-bottom__callback-dropdown').toggleClass('toolbar-bottom__callback-dropdown--close');
  // });
  // $(document).mouseup(function (e) {
  //   var containerMenu = $(".toolbar-bottom__callback-dropdown"),
  //       buttonPink = $(".toolbar-bottom__callback-toggle");
  //   if (containerMenu.has(e.target).length === 0 && buttonPink.has(e.target).length === 0) {
  //     containerMenu.addClass("toolbar-bottom__callback-dropdown--close");
  //     }     
  //   });

});